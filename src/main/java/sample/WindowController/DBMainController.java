package sample.WindowController;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


final class Element {
    private final int id;
    private final String word;
    private final String mean;

    public Element(int id, String word, String mean) {
        this.id = id;
        this.word = word;
        this.mean = mean;
    }

    public int getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getMean() {
        return mean;
    }
}


public class DBMainController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button ExitButton;

    @FXML
    private ListView<String> MyList;

    @FXML
    private TextArea MyMean;

    @FXML
    private Button MySearch;

    @FXML
    private TextField MyTextSearch;

    @FXML
    private Button AddWords;

    @FXML
    public static Connection sqlConnection;

    public static void setSqlConnection(Connection connection) {
        sqlConnection = connection;
    }
    public static Connection getSqlConnection() {
        return sqlConnection;
    }

    @FXML
    public static ArrayList<Element> elements;

    @FXML
    private void opener(Button target, String window) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(String.format("../../Windows/%s", window)));
        try {
            loader.load();

            target.getScene().getWindow().hide();

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ОШИБКА!");
            alert.setHeaderText(String.format("Ошибка при загрузке окна %s", window));
            alert.setContentText(String.valueOf(e));

            alert.showAndWait();
        }
    }

    @FXML
    void initialize() {
        ExitButton.setOnAction(actionEvent -> {
            ExitButton.getScene().getWindow().hide();
        });

        MySearch.setOnAction(actionEvent -> {
            try {
                MyList.getItems().clear();
                elements = new ArrayList<>();

                String like = MyTextSearch.getText();

                Statement statement = getSqlConnection().createStatement();
                String sqlQuery = "SELECT * FROM dict WHERE dict.word ILIKE ?;";
                PreparedStatement preparedStatement = getSqlConnection().prepareStatement(sqlQuery);
                preparedStatement.setString(1, "%" + like + "%");
                try (ResultSet res = preparedStatement.executeQuery()) {
                    while (res.next()) {
                        int id = res.getInt("id");
                        String word = res.getString("word");
                        String mean = res.getString("mean");

                        Element element = new Element(id, word, mean);
                        elements.add(element);
                        MyList.getItems().add(element.getWord());
                    }
                }
            }
            catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR!");
                alert.setHeaderText("Exception");
                alert.setContentText(String.valueOf(e));

                alert.showAndWait();
            }
        });

        AddWords.setOnAction(actionEvent -> {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../../Windows/DBAdd.fxml"));
                loader.load();
                DBAddController controller = loader.<DBAddController>getController();
                DBAddController.setSqlConnection(getSqlConnection());

                opener(AddWords, "DBAdd.fxml");
            }
            catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ОШИБКА!");
                alert.setHeaderText(String.format("Ошибка при загрузке окна %s", "DBAdd.fxml"));
                alert.setContentText(String.valueOf(e));
            }
        });
    }

    @FXML
    public void handleMouseClick(MouseEvent arg0) {
        int index = MyList.getSelectionModel().getSelectedIndex();
        if (index >= 0) {
            String mean = elements.get(index).getMean();

            MyMean.setText(mean);
        }
    }
}
