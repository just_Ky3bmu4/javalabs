package sample.WindowController;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DBConnectionController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField connectionString;

    @FXML
    private PasswordField password;

    @FXML
    private TextField user;

    @FXML
    private Button ExitButton;

    @FXML
    private Button ConnectButton;

    @FXML
    private void opener(Button target, String window) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(String.format("../../Windows/%s", window)));
        try {
            loader.load();

            target.getScene().getWindow().hide();

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ОШИБКА!");
            alert.setHeaderText(String.format("Ошибка при загрузке окна %s", window));
            alert.setContentText(String.valueOf(e));

            alert.showAndWait();
        }
    }

    @FXML
    void initialize() {
        connectionString.setText("jdbc:postgresql://192.168.0.228:5432/javadict");
        user.setText("postgres");
        password.setText("1234");

        ExitButton.setOnAction(actionEvent -> {
            ExitButton.getScene().getWindow().hide();
        });

        ConnectButton.setOnAction(actionEvent -> {
            try {
                Class.forName("org.postgresql.Driver");
                Connection connection = DriverManager.getConnection(connectionString.getText(), user.getText(), password.getText());
                if (connection != null) {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("../../Windows/DBMain.fxml"));
                    loader.load();
                    DBMainController controller = loader.<DBMainController>getController();
                    DBMainController.setSqlConnection(connection);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Success!");
                    alert.setHeaderText("Подключение к БД успешно установленно!");

                    alert.showAndWait();

                    opener(ConnectButton, "DBMain.fxml");
                }
                else {
                    System.out.println("Connection is NOT good!");
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("WARNING!");
                    alert.setHeaderText("Подключение к БД не установлено!\nПопробуйте ещё раз.");

                    alert.showAndWait();
                }
            }
            catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR!");
                alert.setHeaderText("Exception");
                alert.setContentText(String.valueOf(e));

                alert.showAndWait();
            }
        });
    }
}
