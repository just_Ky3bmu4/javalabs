package sample.WindowController;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DBAddController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button ExitButton;

    @FXML
    private Button BackButton;

    @FXML
    private TextArea Mean;

    @FXML
    private TextField Word;

    @FXML
    private Button AddWord;

    @FXML
    public static Connection sqlConnection;

    public static void setSqlConnection(Connection connection) {
        sqlConnection = connection;
    }
    public static Connection getSqlConnection() {
        return sqlConnection;
    }

    @FXML
    private void opener(Button target, String window) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(String.format("../../Windows/%s", window)));
        try {
            loader.load();

            target.getScene().getWindow().hide();

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ОШИБКА!");
            alert.setHeaderText(String.format("Ошибка при загрузке окна %s", window));
            alert.setContentText(String.valueOf(e));

            alert.showAndWait();
        }
    }

    @FXML
    void initialize() {
        ExitButton.setOnAction(actionEvent -> {
            ExitButton.getScene().getWindow().hide();
        });

        BackButton.setOnAction(actionEvent -> {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../../Windows/DBMain.fxml"));
                loader.load();
                DBMainController controller = loader.<DBMainController>getController();
                DBMainController.setSqlConnection(getSqlConnection());

                opener(BackButton, "DBMain.fxml");
            }
            catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ОШИБКА!");
                alert.setHeaderText(String.format("Ошибка при загрузке окна %s", "DBMain.fxml"));
                alert.setContentText(String.valueOf(e));

                alert.showAndWait();
            }
        });

        AddWord.setOnAction(actionEvent -> {
            try {
                String my_word = Word.getText();
                String my_mean = Mean.getText();

                Statement statement = getSqlConnection().createStatement();
                String sqlQuery = "INSERT INTO dict(word,mean) VALUES(?, ?);";
                PreparedStatement preparedStatement = getSqlConnection().prepareStatement(sqlQuery);
                preparedStatement.setString(1, my_word);
                preparedStatement.setString(2, my_mean);
                preparedStatement.executeUpdate();

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success!");
                alert.setHeaderText("Добавление записи успешно завершено!");

                alert.showAndWait();

                Word.clear();
                Mean.clear();
            }
            catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ОШИБКА!");
                alert.setHeaderText("Exception!");
                alert.setContentText(String.valueOf(e));

                alert.showAndWait();
            }
        });
    }
}
